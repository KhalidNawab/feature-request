const { Sequelize } = require("sequelize");

const sequelize = new Sequelize(
  "postgresql://khalidnawab:password@localhost:5432"
);

(async function () {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully");
  } catch (err) {
    console.log(err);
  }
})();
